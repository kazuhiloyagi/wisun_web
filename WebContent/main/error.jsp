<% String str = (String)request.getAttribute("error_message"); %>
<% String encord = (String)request.getAttribute("encord"); %>
<h2><%= str %></h2>
<p class="alert"></p>
<form action="main/error" method="get">
	<input type="hidden" value="<%= encord %>" name="encord" />
	<input type="hidden" value="<%= token.toString() %>" name="token" />
	<input type="hidden" value="<%= src %>" name="src" />
	<input type="hidden" value="logout" name="menu" />
	<input type="submit" value="ログアウト" />
</form>