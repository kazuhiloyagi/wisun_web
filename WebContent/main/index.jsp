<%-- RowSetをResultSetにキャストして使う --%>
<% ResultSet rs = (ResultSet)request.getAttribute("crs"); %>
<%-- 比較用の日付を取得する --%>
<%
  Timestamp from = (Timestamp)session.getAttribute("from");
  Timestamp to = (Timestamp)session.getAttribute("to");
  Timestamp latestDatetime = (Timestamp)application.getAttribute("latestDatetime");
  Timestamp oldestDatetime = (Timestamp)application.getAttribute("oldestDatetime");
  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
%>
<% String encord = (String)request.getAttribute("encord"); %>
<h2>リスト画面</h2>
<p align="right"><%= from %> ～ <%= to %></p>
<table border="1">
	<tr>
		<th>ID</th><th>POWER</th><th>DATETIME</th>
	</tr>
	<% // while文
	while(rs.next()) { %>
	<tr>
		<td><%= rs.getInt("id") %></td>
		<td><%= rs.getFloat("diff_power") %></td>
		<td><%= rs.getTimestamp("measured_at") %></td>
	</tr>
	<% } %>
</table>
<br />
<% if(from.getTime() - (oldestDatetime.getTime() + (1000 * 60 * 60 * 24 * 30) ) > 0) {
	out.println("<form action='main/list' method='get'>");
	out.println("<input type='hidden' value=" + encord + " name='encord' />");
	out.println("<input type='hidden' value=" + token.toString() + " name='token' />");
	out.println("<input type='hidden' value=" + src + " name='src' />");
	out.println("<input type='hidden' value='back' name='menu' />");
    out.println("<input type='submit' value='過去へ'>");
    out.println("</form>");
} %>
<% if(to.getTime() - (latestDatetime.getTime() - (1000 * 60 * 60 * 24 * 30) ) < 0) {
	out.println("<form action='main/list' method='get'>");
	out.println("<input type='hidden' value=" + encord + " name='encord' />");
	out.println("<input type='hidden' value=" + token.toString() + " name='token' />");
	out.println("<input type='hidden' value=" + src + " name='src' />");
	out.println("<input type='hidden' value='forth' name='menu' />");
    out.println("<input type='submit' value='未来へ'>");
    out.println("</form>");
} %>
<br />
<form action="main/list" method="get">
	<input type="hidden" value="<%= encord %>" name="encord" />
	<input type="hidden" value="<%= token.toString() %>" name="token" />
	<input type="hidden" value="<%= src %>" name="src" />
    <input type="hidden" value="logout" name="menu" />
    <input type="submit" value="ログアウト">
</form>