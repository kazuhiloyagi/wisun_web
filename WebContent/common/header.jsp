<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="fw.SimpleKeyGenerator, java.security.Key, java.net.URLEncoder, java.sql.ResultSet, java.sql.Timestamp, java.time.format.DateTimeFormatter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>wisun web</title>
<!-- スタイルシートcssを読み込み -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
<%-- ワンタイムトークンを生成する --%>
<%
  //request.removeAttribute("gkey");
  //request.removeAttribute("src");
  //session.removeAttribute("gkey");
  Key gkey = SimpleKeyGenerator.makeRandomKey(128);
  String src = SimpleKeyGenerator.makeRandomKey(128).toString();
  byte[] token = SimpleKeyGenerator.encode(src.getBytes(), gkey);
  //request.setAttribute("gkey", gkey);
  //request.setAttribute("src", src);
  request.getSession();
  session.setAttribute("gkey", gkey);
%>
</head>
<body>
<%-- ロゴ --%>
<p><a href=""><img src="${pageContext.request.contextPath}/img/logo.jpg" /></a>&nbsp;wisun-web</p>
<%-- ヘッダメニュー --%>
<br />
<div id="main">

