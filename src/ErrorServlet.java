import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * エラー画面遷移のコントローラになるサーブレット ErrorServlet
 */
public class ErrorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ErrorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String path = null;
		HttpSession session= request.getSession(false);
		// 入力パラメータを受け取る
		String menu = request.getParameter("menu");

		// 画面遷移先を決定する
		if (menu == "logout") {
			path = "/login.jsp";

			// セッション属性のloginを消去する
			request.removeAttribute("crs");
			session = request.getSession(false);
			session.removeAttribute("login");
			session.removeAttribute("gkey");
			session.removeAttribute("from");
			session.removeAttribute("to");
			session.getServletContext().removeAttribute("latestDatetime");
			session.getServletContext().removeAttribute("oldestDatetime");
		}
		// 画面遷移を実行する
		request.getRequestDispatcher(path).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String path = null;
		//HttpSession session = request.getSession(false);
		// 入力パラメータを受け取る
		String menu = request.getParameter("menu");

		// 画面遷移先を決定する
		if(menu == "index") {
			path = "/main/error.jsp";
		}
		// 画面遷移を実行する
		request.getRequestDispatcher(path).forward(request, response);
	}
}
