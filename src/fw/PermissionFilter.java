﻿package fw;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.M_accountDAO;
import db.M_accountDTO;
import thread.ThreadLocalContext;

/**
 * Servlet Filter implementation class PermissionFilter
 */
public class PermissionFilter implements Filter {
	HttpSession session;

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		//session.invalidate();
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// ServletRequest を HttpServletRequest にキャストする
		HttpServletRequest req = (HttpServletRequest)request;
		// ServletResponse を HttpServletResponse にキャストする
		HttpServletResponse res =  (HttpServletResponse)response;
		// req と res のエンコーディングを指定する
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/plain; charset=UTF-8");

		try {
			// まだログインしていない場合、login.jspへ遷移する
			session = req.getSession(false);
			if(session == null) {
				req.setAttribute("login_message", "セッション期限が切れました。もう一度ログインしてください");
				RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
				dispatcher.forward(req, res);
			} else if(session.getAttribute("login") == null || Boolean.parseBoolean((String)session.getAttribute("login")) == false){
				req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
				// セッション属性'login'がfalseの場合、login.jspへ遷移する
				RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
				dispatcher.forward(req, res);
			}

			// リクエスト属性の取得
			String account = (String)req.getAttribute("account");
			String password = (String)req.getAttribute("password");
			// データベースへアカウント問い合わせ
			M_accountDAO dao = M_accountDAO.getInstance();

			// ログイン試行回数が10回を超えたら、一時的に(12時間)アカウントをロックする
			int count = Integer.parseInt((String)(session.getAttribute("count")));
			if(count >= 10) {
				if(dao.findByUser_id(account) == 1) {
					// accountが実在した場合は、そのアカウントをロックアウトする
					dao.setLockoutTimeByUser_id(account);
				} else if(dao.findByPassword(password) == 1) {
					// passwordが実在した場合は、そのアカウントをロックアウトする
					dao.setLockoutTimeByPassword(password);
				}
			}

			// アカウントロックされたアカウントがログインしてきたらlogin.jspへ遷移する
			if(dao.findByUser_id(account) == 1) {
				// accountが実在した場合は、そのアカウントをロックアウトする
				if(dao.isLockoutByUser_id(account)) {
					req.setAttribute("login_message", "不正なアクセスです");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				}
			} else if(dao.findByPassword(password) == 1) {
				// passwordが実在した場合は、そのアカウントをロックアウトする
				if(dao.isLockoutByPassword(password)) {
					req.setAttribute("login_message", "不正なアクセスです");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				}
			}

			// セッション属性の取得
			boolean isLogin = Boolean.parseBoolean((String)session.getAttribute("login"));
			// ユーザがログインに成功している場合
			if (isLogin == true) {

				// ログイン追跡パラメータを検証する
				Key skey = (Key)session.getAttribute("skey");
				String source = (String)session.getAttribute("source");
				String enc = (String)req.getParameter("encord");
				if(skey == null || source == null || enc == null) {
					// もしログイン追跡パラメータがnullだったらlogin.jspへ遷移する
					req.setAttribute("login_message", "不正なアクセスです");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else if(SimpleKeyGenerator.decode(enc.getBytes(), skey).toString() != source) {
					// もしログイン追跡パラメータを復号した結果が間違っていたならlogin.jspへ遷移する
					req.setAttribute("login_message", "不正なアクセスです");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				}

				// ログイン試行回数を無効化
				session.removeAttribute("count");

				// それまで使っていたセッションIDを無効にし、新しいセッションIDを発行する
				session.invalidate();
				session = req.getSession(true);

				// セッション変数に「ログイン済み」であることを記録する
				session.setAttribute("login", isLogin);

				// ログイン追跡パラメータを発行する
				if (account != null && password != null) {
					skey = SimpleKeyGenerator.makeRandomKey(128);
					source = account + password;
					source = SimpleKeyGenerator.encode(source.getBytes(), skey).toString();
					skey = SimpleKeyGenerator.makeRandomKey(128);
					byte[] encord = SimpleKeyGenerator.encode(source.getBytes(), skey);
					req.setAttribute("encord", encord.toString());
					session.setAttribute("source",source);
					session.setAttribute("skey", skey);
				}

				// データベースからユーザー情報の取得
				M_accountDAO adao;
				ArrayList<M_accountDTO> list;
				boolean isVerified = false;
				if (account != null && password != null) {
					adao = M_accountDAO.getInstance();
					list = adao.getOne(account, password);
					Iterator<M_accountDTO> it = list.iterator();
					while(it.hasNext()) {
						M_accountDTO adto = it.next();
						isVerified = adto.isVerified();
					}
					// もしユーザーが認証済みユーザーであれば
					if (isVerified) {
						//req.setAttribute("account", account);
						//req.setAttribute("password", password);
						req.setAttribute("verified", isVerified);

						// ページ遷移
						RequestDispatcher dispatcher = req.getRequestDispatcher("/main/list");
						dispatcher.forward(req, res);
					} else {
						// もしユーザーが認証済みユーザーでなければ、error.jspへ遷移する
						req.setAttribute("error_message", "ACCESS DENIED");
						RequestDispatcher dispatcher = req.getRequestDispatcher("/main/error");
						dispatcher.forward(req, res);
					}
				} else {
					// もしリクエスト属性に不備があれば、login.jspへ遷移する
					req.setAttribute("login_message", "セッション期限が切れました。もう一度ログインしてください");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				}
			}

		} catch(Exception ignore) {  // エラーの場合の記述のみ
			// 何もしない
		} finally {
			// ThreadLocal データの削除
	        ThreadLocalContext.destroy();
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		//session.invalidate();
	}
}
