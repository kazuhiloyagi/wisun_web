﻿package fw;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.DBFormatter;
import db.M_accountDAO;
import thread.ThreadLocalContext;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class LoginFilter implements Filter {
	HttpSession session;

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		//session = null;
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// ServletRequest を HttpServletRequest にキャストする
		HttpServletRequest req = (HttpServletRequest)request;
		// ServletResponse を HttpServletResponse にキャストする
		HttpServletResponse res =  (HttpServletResponse)response;
		// req と res のエンコーディングを指定する
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/plain; charset=UTF-8");

		// リクエスト属性を初期化する
		req.removeAttribute("login_message");
		req.removeAttribute("error_message");
		req.removeAttribute("crs");

		try {
			// ワンタイムトークンを検証する
			session = req.getSession(true);
			//		Key gkey = (Key)session.getAttribute("gkey");
			//		session.removeAttribute("gkey");
			//		String src = (String)req.getParameter("src");
			//		String token = (String)req.getParameter("token");
			//		if(gkey == null || src == null || token == null) {
			//			// もしワンタイムトークンがnullだったらlogin.jspへ遷移する
			//			req.setAttribute("login_message", "不正なアクセスです");
			//			RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
			//			dispatcher.forward(req, res);
			//		} else if(SimpleKeyGenerator.decode(token.getBytes(), gkey).toString() != src) { // エラーになる
			//			// もしワンタイムトークンを復号した結果が間違っていたならlogin.jspへ遷移する
			//			req.setAttribute("login_message", "不正なアクセスです");
			//			RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
			//			dispatcher.forward(req, res);
			//		}

			// まだログインしていない場合、まだ session は発行されていない
			//if(session == null) {
			if(session.getAttribute("count") == null) {
				// ログイン試行回数を記録する
				session.setAttribute("count", 1);

				// 入力パラメータの取得
				String account = null;
				String password = null;
				DBFormatter formatter = new DBFormatter();
				account = (String)req.getParameter("account");
				account = formatter.formatSqlCharacter(account);
				if(account == null || account.length() == 0) {
					// もしログインアカウントがnullだったらlogin.jspへ遷移する
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					req.setAttribute("account", account);
				}
				password = (String)req.getParameter("password");
				password = formatter.formatSqlCharacter(password);
				if(password == null || password.length() == 0) {
					// もしログインパスワードがnullだったらlogin.jspへ遷移する
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					req.setAttribute("password", password);
				}

				// データベースへログイン問い合わせ
				M_accountDAO adao = M_accountDAO.getInstance();
				int num = adao.findOne(account, password);
				if(num == 1) {
					//セッション属性の取得
					session = req.getSession(true);
					session.setAttribute("login", true);
				}
	
				//ページ遷移
				// sessionが無い、またはログインフラグが取得できなければlogin.jspへ遷移する
				if(session == null) {
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else if(session.getAttribute("login") == null) {
					session.invalidate();
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					// pass the request along the filter chain
					chain.doFilter(request, response);
				}

			} else 	if(req.getSession(false) != null && Integer.parseInt((String)(req.getSession(false).getAttribute("count"))) > 1) {
				// すでに session が発行されている場合
				HttpSession newSession = req.getSession(false);
				// ログイン試行回数を記録する
				int count = Integer.parseInt((String)(newSession.getAttribute("count")));
				newSession.setAttribute("count", (count + 1));

				// もし既存のセッションIDと違っていたらlogin.jspへ遷移する
				if(newSession.getId() != session.getId()) {
					newSession.invalidate();
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else if(newSession.getAttribute("login") == null) {
					// もしログインフラグが取得できなければlogin.jspへ遷移する
					newSession.invalidate();
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					// pass the request along the filter chain
					chain.doFilter(request, response);
					// ThreadLocal データの削除

				}

			} else 	if(session != null && Integer.parseInt((String)(session.getAttribute("count"))) > 1) {
				// すでに session が発行されている場合 (※ここには来ないかもしれない)
				// ログイン試行回数を記録する
				int count = Integer.parseInt((String)(session.getAttribute("count")));
				session.setAttribute("count", (count + 1));

				if(session.getAttribute("login") == null) {
					// もしログインフラグが取得できなければlogin.jspへ遷移する
					session.invalidate();
					req.setAttribute("login_message", "ユーザー名またはパスワードが無効です");
					RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
					dispatcher.forward(req, res);
				} else {
					// pass the request along the filter chain
					chain.doFilter(request, response);
				}
			}

		} catch(Exception ignore) {  // エラーの場合の記述のみ
			// 何もしない
		} finally {
			// ThreadLocal データの削除
	        ThreadLocalContext.destroy();
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		//session = null;
	}
}
