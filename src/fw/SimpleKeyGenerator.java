﻿package fw;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;

public class SimpleKeyGenerator {
	 /** 初期化ベクトル */
    private static byte[] IV = { 0x1c, 0x12, 0x1e, 0x10, 0x17, 0x1f, 0x1a, 0x13, 0x19, 0x15, 0x11, 0x16, 0x18, 0x1b, 0x14, 0x1d };

	/**
	 * 秘密鍵をランダムに生成する
	 * @param key_bits 鍵の長さ（ビット単位）
	 * @return 秘密鍵オブジェクト
	 */
	public static Key makeRandomKey(int key_bits) {
		try {
			KeyGenerator generator = KeyGenerator.getInstance("AES");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			generator.init(key_bits, random);

			return generator.generateKey();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 文字列を暗号化する
	 * @param src 暗号化する対象のバイト列
	 * @param skey 暗号を初期化するためのキー
	 * @return 暗号化を施したバイト文字列
	 */
	public static byte[] encode(byte[] src, Key skey) {
		try {
			Cipher cipher = Cipher.getInstance("/AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skey, new IvParameterSpec(IV));
			return cipher.doFinal(src);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 暗号から元の文字列を復号化する
	 * @param src 暗号化する対象のバイト列
	 * @param skey 暗号を初期化するためのキー
	 * @return 暗号化を施したバイト文字列
	 */
	public static byte[] decode(byte[] src, Key skey) {
		try {
			Cipher cipher = Cipher.getInstance("/AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, skey, new IvParameterSpec(IV));
			return cipher.doFinal(src);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}