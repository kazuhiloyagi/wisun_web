package db;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBFormatter {
	/**
	 * 入力されたHTMLのエスケープ処理をする
	 * @param text 処理対象の文字列
	 * @return エスケープ処理後の文字列
	 */
	public String escapeHtmlTags(String text) {
		// タグの変換
		text = text.replaceAll("/ /", "&nbsp;");
		text = text.replaceAll("/</", "&lt;");
		text = text.replaceAll("/>/", "&gt;");
		text = text.replaceAll("/\t/", "    ");
		text = text.replaceAll("\\t", "    ");
		text = text.replaceAll("/\\t/", "    ");
		// 改行文字の変換
		text = text.replaceAll("/\n/", "<br />");
		text = text.replaceAll("/\\n/", "<br />");
		text = text.replaceAll("/\r/", "<br />");
		text = text.replaceAll("/\\r/", "<br />");
		return text;
	}

	/**
	 * DB挿入前にエスケープ処理をする
	 * @param text 処理対象の文字列
	 * @return エスケープ処理後の文字列
	 */
	public String formatSqlCharacter(String text) {
		text = escapeHtmlTags(text);
		// ", ' の変換
		text = text.replaceAll("/'/", "&#39;");
		text = text.replaceAll("/\"/", "&#34;");
		// ; の変換
		text = text.replaceAll("/;/", "&#59;");
		// -- の変換
		text = text.replaceAll("/--/", "&#45;&#45;");
		// \ の変換
		text = text.replaceAll("/\\/", "&#92;");
		text = text.replaceAll("/%5c/", "&#92;");
		text = text.replaceAll("/&#x5c;/", "&#92;");
		// %00 の変換
		text = text.replaceAll("/%00/", "&#37;00");
		text = text.replaceAll("/&#x00;/", "&#37;00");
		// file:// の禁止
		text = text.replaceAll("/file:\\/\\//", "");
		// 1=1 の禁止
		text = text.replaceAll("/1=1/", "");

		return text;
	}

	/**
	 * DB挿入前にメールアドレスの書式を検査する
	 * @param text 処理対象のメールアドレス文字列
	 * @return メールアドレス文字列の書式が正しければtrueを返す
	 */
	public boolean checkMailaddressFormat(String text) {
		text = formatSqlCharacter(text);

		String aText = "[\\w!#%&'/=~`\\*\\+\\?\\{\\}\\^\\$\\-\\|]";
		String dotAtom = aText + "+" + "(\\." + aText + "+)*";
		String reg = "^" + dotAtom + "@" + dotAtom + "$";

		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher(text);
		if(matcher.find()) {
			//String group = matcher.group();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * DB挿入前にパスワードの内容を検査する
	 * @param password 処理対象のパスワード文字列
	 * @param account パスワードと紐づいたユーザID
	 * @param lastPassword 前回のパスワード
	 * @return パスワード文字列の書式が正しければtrueを返す
	 */
	public boolean passwordFilter(String password, String account, String lastPassword) {
		// ユーザIDが含まれるものは許可しない
		String reg1 = "/" + account + "/";
		Pattern pattern1 = Pattern.compile(reg1);
		Matcher matcher1 = pattern1.matcher(password);
		if(matcher1.find()) {
			return false;
		}
		// 生年月日が含まれるものは許可しない
		String reg2 = "^(\\d{4})(-?)(0[1-9]|1[0-2])(-?)(0[1-9]|[12][0-9]|3[01])$";
		Pattern pattern2 = Pattern.compile(reg2);
		Matcher matcher2 = pattern2.matcher(password);
		if(matcher2.find()) {
			return false;
		}
		// 8文字以上で英字と数字と記号がそれぞれ1文字以上含まれるものを許可する
		String reg3 = "[(\\d+)(\\w+)(=*)(-*)(<*)(>*)(;*)(:*)(@*)(\\.*)(\\$*)(\\**)(\\+*)(\\?*)(\\^*)(\\|*)(\\{*)(\\}*)(\\{*)(\\}*)(\\(*)(\\)*)]{8,}";
		Pattern pattern3 = Pattern.compile(reg3);
		Matcher matcher3 = pattern3.matcher(password);
		if(matcher3.find()) {
			return false;
		}
		// 前回と同じパスワードは許可しない
		String reg4 = "/" + lastPassword + "/";
		Pattern pattern4 = Pattern.compile(reg4);
		Matcher matcher4 = pattern4.matcher(password);
		if(matcher4.find()) {
			return false;
		}

		return true;
	}
}
