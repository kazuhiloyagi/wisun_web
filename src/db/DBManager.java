﻿package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sun.rowset.CachedRowSetImpl;

//import javax.naming.InitialContext;
//import javax.sql.DataSource;

public class DBManager {
	/**
	 * DBと接続するクラスメソッド
	 * @return DBと接続済みのコネクションオブジェクト
	 */
	public static Connection getConnection() {
		try {
//			// InitialContextオブジェクトを取得
//			InitialContext ctx = new InitialContext();
//			// InitialContextのlookup()メソッドを呼んで、DataSourceオブジェクトを取得
//			DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/chuden");
//			// DataSourceからConnectionオブジェクトを取得
//			Connection con = ds.getConnection();
//			return con;
			Class.forName("org.postgresql.Driver"); // これを記述しないとEclipseから PostgreSQLのドライバーが見つからない
			Connection con = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/dbname", // 接続先文字列
					"dbuser", //DBユーザーID
					"dbpassword" // DBパスワード
					);
			return con;

		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 検索SQLを発行して、結果をCachedRowSetImplに入れて返す。
	 * @param sql SQL文字列
	 * @return 検索結果のレコードが入ったRowSetオブジェクト
	 */
	public static CachedRowSetImpl simpleFind(String sql) throws SQLException  {
		Connection con = null;
		Statement smt = null;

		try {
			con = getConnection();
			smt = con.createStatement();
			ResultSet rs = smt.executeQuery(sql);
			CachedRowSetImpl crs = new CachedRowSetImpl();
			crs.populate(rs);

			return crs; // 検索結果のレコードが返ってくる

		} finally {
			if (smt != null) {
				try {
					smt.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

	/**
	 * 検索SQLを発行して、結果をCachedRowSetImplに入れて返す。
	 * @param sql SQL文字列
	 * @param arg1 プリペアドステートメントの文字列パラメータ
	 * @return 検索結果のレコードが入ったRowSetオブジェクト
	 */
	public static CachedRowSetImpl simpleFind(String sql, String arg1) throws SQLException  {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, arg1);

			ResultSet rs = ps.executeQuery();
			CachedRowSetImpl crs = new CachedRowSetImpl();
			crs.populate(rs);

			return crs; // 検索結果のレコードが返ってくる

		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

	/**
	 * 検索SQLを発行して、結果をCachedRowSetImplに入れて返す。
	 * @param sql SQL文字列
	 * @param arg1 プリペアドステートメントの文字列パラメータ
	 * @param arg2 プリペアドステートメントの文字列パラメータ
	 * @return 検索結果のレコードが入ったRowSetオブジェクト
	 */
	public static CachedRowSetImpl simpleFind(String sql, String arg1, String arg2) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, arg1);
			ps.setString(2, arg2);

			ResultSet rs = ps.executeQuery();
			CachedRowSetImpl crs = new CachedRowSetImpl();
			crs.populate(rs);

			return crs; // 検索結果のレコードが返ってくる

		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

	/**
	 * 更新SQLを発行して、更新したレコード数を返す。
	 * @param sql SQL文字列
	 * @param arg1 プリペアドステートメントの文字列パラメータ
	 * @param arg2 プリペアドステートメントの文字列パラメータ
	 * @param arg3 プリペアドステートメントの文字列パラメータ
	 * @return 更新が成功したレコード数
	 */
	public static int simpleUpdate(String sql, String arg1, String arg2, String arg3) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, arg1);
			ps.setString(2, arg2);
			ps.setString(3, arg3);
			return ps.executeUpdate();  // 更新レコード数が返ってくる

		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException ignore) {

				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ignore) {

				}
			}
		}
	}

	public static void main(String[] args) throws SQLException {
		Connection con = getConnection();
		System.out.println("con=" + con);
		con.close();
	}
}
