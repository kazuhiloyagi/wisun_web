﻿package db;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.sun.rowset.CachedRowSetImpl;

import thread.Destroyable;

/**
 * eaテーブルおよびその代理であるM_eaDTOに対応するDAO(Data Access Object)
 */
public class M_eaDAO implements Destroyable {
	// ThreadLocal クラスを使って複数スレッドでデータが扱われるのを防ぐ
	private static ThreadLocal<M_eaDAO> holder = new ThreadLocal<M_eaDAO>() {
        @Override
        public M_eaDAO initialValue() {
            return new M_eaDAO();
        }
    };

	// Singleton パターン
	private M_eaDAO(){	}
	public static M_eaDAO getInstance(){
		return holder.get();
	}

	/**
	 * 全件検索を行う
	 * @return 検索でヒットしたレコードが入ったCachedRowSetImpl
	 */
	public CachedRowSetImpl findAll() {
		String sql = "SELECT diff_power, measured_at FROM ea ORDER BY measured_at";

		try {
			return DBManager.simpleFind(sql);

		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 期間検索を行う
	 * @param from 検索期間の始めを表すLocalDateTime
	 * @param to 検索期間の終わりを表すLocalDateTime
	 * @return 検索でヒットしたレコードが入ったCachedRowSetImpl
	 */
	public CachedRowSetImpl findPeriod(LocalDateTime from, LocalDateTime to) {
		String sql = "SELECT diff_power, measured_at FROM ea WHERE measured_at BETWEEN ? AND ?";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
		String fromStr = from.format(formatter);
		String toStr = to.format(formatter);

		try {
			return DBManager.simpleFind(sql, fromStr, toStr);

		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 最新のレコードを1件だけ検索を行う
	 * @return 検索でヒットしたレコードが入ったCachedRowSetImpl
	 */
	public CachedRowSetImpl findLatest() {
		String sql = "SELECT diff_power, measured_at FROM ea ORDER BY measured_at DESC LIMIT 1";

		try {
			return DBManager.simpleFind(sql);
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 最古のレコードを1件だけ検索を行う
	 * @return 検索でヒットしたレコードが入ったCachedRowSetImpl
	 */
	public CachedRowSetImpl findOldest() {
		String sql = "SELECT diff_power, measured_at FROM ea ORDER BY measured_at LIMIT 1";

		try {
			return DBManager.simpleFind(sql);
		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Destroyableインターフェイスのメソッドの実装
	 * ThreadLocal クラスの解放を行う
	 */
	public void destroy() {

	}
}
