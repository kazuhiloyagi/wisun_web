package db;

import java.sql.Timestamp;

/**
 * M_accountテーブルに対応するDTO(Data Transfer Object)
 */
public class M_accountDTO {
	private String user_id;
	private String password;
	private String email;
	private boolean verified;
	private Timestamp update_date;
	private Timestamp lockout_until_this_datetime;

	// Setter & Getter
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		DBFormatter formatter = new  DBFormatter();
		this.user_id = formatter.formatSqlCharacter(user_id);  // SQLエスケープする
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		DBFormatter formatter = new  DBFormatter();
		this.password = formatter.formatSqlCharacter(password);  // SQLエスケープする
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		DBFormatter formatter = new  DBFormatter();
		this.email = formatter.formatSqlCharacter(email);  // SQLエスケープする
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public Timestamp getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Timestamp update_date) {
		this.update_date = update_date;
	}
	public Timestamp getLockout_until_this_datetime() {
		return lockout_until_this_datetime;
	}
	public void setLockout_until_this_datetime(Timestamp lockout_until_this_datetime) {
		this.lockout_until_this_datetime = lockout_until_this_datetime;
	}
}
