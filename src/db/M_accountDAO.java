﻿package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import com.sun.rowset.CachedRowSetImpl;

import thread.Destroyable;

/**
 * M_accountテーブルおよびその代理であるM_accountDTOに対応するDAO(Data Access Object)
 */
public class M_accountDAO implements Destroyable {
	// ThreadLocal クラスを使って複数スレッドでデータが扱われるのを防ぐ
	private static ThreadLocal<M_accountDAO> holder = new ThreadLocal<M_accountDAO>() {
        @Override
        public M_accountDAO initialValue() {
            return new M_accountDAO();
        }
    };

	// Singleton パターン
	private M_accountDAO(){	}
	public static M_accountDAO getInstance(){
		return holder.get();
	}

	// データベースの検索結果(加工済み)を入れるリスト
	private ArrayList<M_accountDTO> list;
	// データベースの検索結果(未加工)を入れるCachedRowSetImpl
	private CachedRowSetImpl crs;
	// 問い合わせ戻り値用のリスト
	private ArrayList<M_accountDTO> al;

	/**
	 * 1件検索を行う
	 * @param user_id ユーザID
	 * @param password パスワード
	 * @return 検索でヒットした件数
	 */
	public int findOne(String user_id, String password) {
		String sql = "SELECT * FROM m_account WHERE user_id = ? AND password = ?";
		DBFormatter formatter = new  DBFormatter();
		user_id = formatter.formatSqlCharacter(user_id);  // SQLエスケープする
		password =  formatter.formatSqlCharacter(password);

		try {
			crs = DBManager.simpleFind(sql, user_id, password);
			ResultSet rs = (ResultSet)crs;
			while(rs.next()) {
				M_accountDTO adto = new M_accountDTO();
				adto.setUser_id(rs.getString("user_id"));
				adto.setPassword(rs.getString("password"));
				adto.setEmail(rs.getString("email"));
				adto.setVerified(rs.getBoolean("verified"));
				adto.setUpdate_date(rs.getTimestamp("update_date"));
				list.add(adto);
			}

			return crs.size();

		} catch (SQLException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 1件検索を行う
	 * @param user_id ユーザID
	 * @param password パスワード
	 * @return 検索でヒットしたリスト
	 */
	public ArrayList<M_accountDTO> getOne(String user_id, String password) {
		// 戻り値用のリスト
		al = null;

		if(list.isEmpty()) {
			findOne(user_id, password);
		}
		// listのuser_idとpasswordが合っているか調べる
		Iterator<M_accountDTO> it = list.iterator();
		while(it.hasNext()) {
			M_accountDTO adto = it.next();
			if(adto.getUser_id() == user_id && adto.getPassword() == password) {
				// もし、user_idとpasswordが合っていたら、alに加える
				al.add(adto);
			}
		}
		// java.util.stream.Streamでの書き方 (java コードビルダー1.8以上が必要)
		//List<M_accountDTO> result = list.stream().filter(adto -> adto.getUser_id().equals(user_id)).collect(Collectors.toList());
		return al;
	}

	/**
	 * user_idのみで1件検索を行う
	 * @param user_id ユーザID
	 * @return 検索でヒットした件数
	 */
	public int findByUser_id(String user_id) {
		String sql = "SELECT * FROM m_account WHERE user_id = ?";
		DBFormatter formatter = new  DBFormatter();
		user_id = formatter.formatSqlCharacter(user_id);  // SQLエスケープする

		// 戻り値用のリスト
		al = null;

		// listのuser_idが合っているか調べる
		Iterator<M_accountDTO> it = list.iterator();
		while(it.hasNext()) {
			M_accountDTO dto = it.next();
			if(dto.getUser_id() == user_id) {
				// もし、user_idが合っていたら、alに加える
				al.add(dto);
			}
		}
		if(al == null) {
			try {
				crs = DBManager.simpleFind(sql, user_id);
				ResultSet rs = (ResultSet)crs;
				while(rs.next()) {
					M_accountDTO adto = new M_accountDTO();
					adto.setUser_id(rs.getString("user_id"));
					adto.setPassword(rs.getString("password"));
					adto.setEmail(rs.getString("email"));
					adto.setVerified(rs.getBoolean("verified"));
					adto.setUpdate_date(rs.getTimestamp("update_date"));
					list.add(adto);
					al.add(adto);
				}

			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		if(al == null) {
			return 0;
		} else {
			return al.size();
		}
	}

	/**
	 * user_idのみでアカウントにロックアウト解除期限を与える
	 * @param user_id ユーザID
	 */
	public void setLockoutTimeByUser_id(String user_id) {
		// alのuser_idが合っているか調べる
		Iterator<M_accountDTO> it = al.iterator();
		while(it.hasNext()) {
			M_accountDTO adto = it.next();
			if(adto.getUser_id() == user_id) {
				// もし、user_idが合っていたら、アカウントにロックアウト解除期限を与える
				LocalDateTime lockout = LocalDateTime.now().plusHours(12);
				// LocalDateTimeからjava.sql.Timestampへの変換に使用する
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
				Timestamp tmt = Timestamp.valueOf(lockout.format(formatter));
				adto.setLockout_until_this_datetime(tmt);
			}
		}
	}

	/**
	 * user_idのみでアカウントがロックアウト対象かどうかを返す
	 * @param user_id ユーザID
	 */
	public boolean isLockoutByUser_id(String user_id) {
		// 戻り値
		boolean result = false;
		// alのuser_idが合っているか調べる
		Iterator<M_accountDTO> it = al.iterator();
		while(it.hasNext()) {
			M_accountDTO adto = it.next();
			if(adto.getUser_id() == user_id) {
				// もし、user_idが合っていたら、アカウントをロックアウトする
				Timestamp tmt = adto.getLockout_until_this_datetime();
				LocalDateTime lockout = tmt.toLocalDateTime();
				if(lockout.compareTo(LocalDateTime.now()) >= 0) {
					result = true;
				} else {
					adto.setLockout_until_this_datetime(null);
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * passwordのみで1件検索を行う
	 * @param password パスワード
	 * @return 検索でヒットした件数
	 */
	public int findByPassword(String password) {
		String sql = "SELECT * FROM m_account WHERE password = ?";
		DBFormatter formatter = new  DBFormatter();
		password = formatter.formatSqlCharacter(password);  // SQLエスケープする

		// 戻り値用のリスト
		al = null;

		// listのpasswordが合っているか調べる
		Iterator<M_accountDTO> it = list.iterator();
		while(it.hasNext()) {
			M_accountDTO dto = it.next();
			if(dto.getPassword() == password) {
				// もし、passwordが合っていたら、alに加える
				al.add(dto);
			}
		}
		if(al == null) {
			try {
				crs = DBManager.simpleFind(sql, password);
				ResultSet rs = (ResultSet)crs;
				while(rs.next()) {
					M_accountDTO adto = new M_accountDTO();
					adto.setUser_id(rs.getString("user_id"));
					adto.setPassword(rs.getString("password"));
					adto.setEmail(rs.getString("email"));
					adto.setVerified(rs.getBoolean("verified"));
					adto.setUpdate_date(rs.getTimestamp("update_date"));
					list.add(adto);
					al.add(adto);
				}

			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		if(al == null) {
			return 0;
		} else {
			return al.size();
		}
	}

	/**
	 * passwordのみでアカウントにロックアウト解除期限を与える
	 * @param password パスワード
	 */
	public void setLockoutTimeByPassword(String password) {
		// alのpasswordが合っているか調べる
		Iterator<M_accountDTO> it = al.iterator();
		while(it.hasNext()) {
			M_accountDTO adto = it.next();
			if(adto.getPassword() == password) {
				// もし、user_idが合っていたら、アカウントにロックアウト解除期限を与える
				LocalDateTime lockout = LocalDateTime.now().plusHours(12);
				// LocalDateTimeからjava.sql.Timestampへの変換に使用する
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
				Timestamp tmt = Timestamp.valueOf(lockout.format(formatter));
				adto.setLockout_until_this_datetime(tmt);
			}
		}
	}

	/**
	 * passwordのみでアカウントがロックアウト対象かどうかを返す
	 * @param password パスワード
	 */
	public boolean isLockoutByPassword(String password) {
		// 戻り値
		boolean result = false;
		// alのuser_idが合っているか調べる
		Iterator<M_accountDTO> it = al.iterator();
		while(it.hasNext()) {
			M_accountDTO adto = it.next();
			if(adto.getPassword() == password) {
				// もし、user_idが合っていたら、アカウントをロックアウトする
				Timestamp tmt = adto.getLockout_until_this_datetime();
				LocalDateTime lockout = tmt.toLocalDateTime();
				if(lockout.compareTo(LocalDateTime.now()) >= 0) {
					result = true;
				} else {
					adto.setLockout_until_this_datetime(null);
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * 引数を元にINSERT処理を行う
	 * @param user_id ユーザID
	 * @param password パスワード
	 * @param email メールアドレス
	 * @return レコード作成に成功したレコード数
	 */
	public int insert(String user_id, String password, String email) {
		int result =0;
		String sql = "INSERT INTO m_account(user_id, password, email) VALUES(?, ?, ?)";
		DBFormatter formatter = new  DBFormatter();
		user_id = formatter.formatSqlCharacter(user_id);  // SQLエスケープする
		password =  formatter.formatSqlCharacter(password);

		// メールアドレスは書式をチェックする
		if(formatter.checkMailaddressFormat(email)) {
			email = formatter.formatSqlCharacter(email);
			try {
				result = DBManager.simpleUpdate(sql, user_id, password, email);  // 更新レコード数が返ってくる
				if (result == 1) {
					findOne(user_id, password);
				}

			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		return result;
	}

	/**
	 * Destroyableインターフェイスのメソッドの実装
	 * ThreadLocal クラスの解放を行う
	 */
	public void destroy() {
		list = null;
		crs = null;
		al = null;
	}
}
