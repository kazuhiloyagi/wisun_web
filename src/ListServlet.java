import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.rowset.CachedRowSetImpl;

import db.M_eaDAO;
import thread.ThreadLocalContext;

/**
 * リスト画面遷移のコントローラになるサーブレット ListServlet
 */
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String path = null;
		HttpSession session= request.getSession(false);
		// 入力パラメータを受け取る
		String menu = request.getParameter("menu");
		// LocalDateTimeからjava.sql.Timestampへの変換に使用する
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");

		// 画面遷移先を決定する
		if(menu == "back") {
			path = "/main/index.jsp";

			// まずセッション属性があれば、それを再表示する
			LocalDateTime to = ((Timestamp)session.getAttribute("to")).toLocalDateTime();
			LocalDateTime from = ((Timestamp)session.getAttribute("from")).toLocalDateTime();
			LocalDateTime oldestDatetime = from.minusMonths(1);

			// データベースへデータ問い合わせ
			M_eaDAO dao = M_eaDAO.getInstance();
			CachedRowSetImpl oldest = dao.findOldest();
			ResultSet rs = (ResultSet)oldest;
			try {
				while(rs.next()) {
					oldestDatetime = (rs.getTimestamp("measured_at")).toLocalDateTime();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// もしデータベースに保管されている最古の日付より古かった場合は、最古の日付に差し替える
			if(from.minusMonths(1).compareTo(oldestDatetime) < 0) {
				from = oldestDatetime;
			} else {
				from = from.minusMonths(1);
			}
			to = from.plusMonths(1);

			// リクエスト属性にCachedRowSetImplを渡す
			CachedRowSetImpl crs = dao.findPeriod(from, to);
			request.setAttribute("crs", crs);
			session.setAttribute("from", from);
			session.setAttribute("to", to);
			session.getServletContext().setAttribute("oldestDatetime", oldestDatetime.format(formatter));

		} else if(menu == "forth") {
			path = "/main/index.jsp";

			// まずセッション属性があれば、それを再表示する
			LocalDateTime to = ((Timestamp)session.getAttribute("to")).toLocalDateTime();
			LocalDateTime from = ((Timestamp)session.getAttribute("from")).toLocalDateTime();
			LocalDateTime latestDatetime = to.plusMonths(1);

			// データベースへデータ問い合わせ
			M_eaDAO dao = M_eaDAO.getInstance();
			CachedRowSetImpl latest = dao.findLatest();
			ResultSet rs = (ResultSet)latest;
			try {
				while(rs.next()) {
					latestDatetime = (rs.getTimestamp("measured_at")).toLocalDateTime();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			// もしデータベースに保管されている最新の日付より新しかった場合は、最新の日付に差し替える
			if(to.plusMonths(1).compareTo(latestDatetime) > 0) {
				to = latestDatetime;
			} else {
				to = to.plusMonths(1);
			}
			from = to.minusMonths(1);

			// リクエスト属性にCachedRowSetImplを渡す
			CachedRowSetImpl crs = dao.findPeriod(from, to);
			request.setAttribute("crs", crs);
			session.setAttribute("from", from.format(formatter));
			session.setAttribute("to", to.format(formatter));
			session.getServletContext().setAttribute("latestDatetime", latestDatetime.format(formatter));

		} else if (menu == "logout") {
			path = "/login.jsp";

			// セッション属性のloginを消去する
			request.removeAttribute("crs");
			session = request.getSession(false);
			session.removeAttribute("login");
			session.removeAttribute("gkey");
			session.removeAttribute("from");
			session.removeAttribute("to");
			session.getServletContext().removeAttribute("latestDatetime");
			session.getServletContext().removeAttribute("oldestDatetime");

			// ThreadLocal データの削除
	        ThreadLocalContext.destroy();
		}
		// 画面遷移を実行する
		request.getRequestDispatcher(path).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String path = null;
		HttpSession session = request.getSession(false);
		// 入力パラメータを受け取る
		String menu = request.getParameter("menu");
		// LocalDateTimeからjava.sql.Timestampへの変換に使用する
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");

		// 画面遷移先を決定する
		if(menu == "index") {
			path = "/main/index.jsp";

			LocalDateTime to;
			LocalDateTime from;
			LocalDateTime latestDatetime;

			// データベースへデータ問い合わせ
			M_eaDAO dao = M_eaDAO.getInstance();
			if(session.getAttribute("to") == null || session.getAttribute("from") == null) {
				// デフォルトで直近1か月間のデータを表示する
				to = LocalDateTime.now();
				latestDatetime = to;
				// データベースに保管されている最新の日付がある場合はそれに差し替える
				CachedRowSetImpl latest = dao.findLatest();
				ResultSet rs = (ResultSet)latest;
				try {
					while(rs.next()) {
						latestDatetime = (rs.getTimestamp("measured_at")).toLocalDateTime();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

				if(to.compareTo(latestDatetime) > 0) {
					to = latestDatetime;
				} else {
					//to = to;
				}
				from = to.minusMonths(1);
				session.getServletContext().setAttribute("latestDatetime", latestDatetime.format(formatter));

			} else {
				// もしセッション属性があれば、それを再表示する
				to = ((Timestamp)session.getAttribute("to")).toLocalDateTime();
				from = ((Timestamp)session.getAttribute("from")).toLocalDateTime();
			}

			// リクエスト属性にCachedRowSetImplを渡す
			CachedRowSetImpl crs = dao.findPeriod(from, to);
			request.setAttribute("crs", crs);
			session.setAttribute("from", from.format(formatter));
			session.setAttribute("to", to.format(formatter));

		}
		// 画面遷移を実行する
		request.getRequestDispatcher(path).forward(request, response);
	}

}
