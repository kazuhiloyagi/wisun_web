package thread;

public interface Destroyable {
	public void destroy();
}
