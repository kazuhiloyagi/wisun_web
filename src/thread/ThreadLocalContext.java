package thread;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * データの解放を容易にするためにスレッドローカルを一元的に管理するクラス
 */
public class ThreadLocalContext {
	private static ThreadLocal<Map<Class<?>, Destroyable>> threadTable = new ThreadLocal<Map<Class<?>, Destroyable>>()
    {
        @Override
        public Map<Class<?>, Destroyable> initialValue()
        {
            return new HashMap<Class<?>, Destroyable>();
        }
    };

    /**
     * スレッドローカルなデータを削除する
     */
    public static void destroy()
    {
        Map<Class<?>, Destroyable> map = threadTable.get();
        Iterator<Destroyable> it = map.values().iterator();
        while(it.hasNext())
        {
            Destroyable obj = it.next();
            obj.destroy();
        }
        threadTable.remove();
    }

    public static <T extends Destroyable> void put(Class<T> key, T value)
    {
        threadTable.get().put(key, value);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Destroyable> T get(Class<T> key)
    {
        return (T)threadTable.get().get(key);
    }
}

