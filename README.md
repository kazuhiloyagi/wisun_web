# README #

[wisun_classプロジェクト](https://bitbucket.org/kazuhiloyagi/wisun_class)で取得したスマートメーターの消費電力値を表示するWebアプリケーションです。
Java Servlet を Apache Tomcatで動かします。(Eclipse で作成した project です。)

## What is this repository for? ##

* 取得した消費電力値をPostgreSQLから取得します。
* ログイン認証が通ったユーザーのみ、消費電力量データを閲覧できます。
* Current Version 0.1
* MIT License

## How do I get set up? ##
### Deployment instructions
本プロジェクトをインストールしたい適当なディレクトリで、次の手順でインストールします。
```
$ git clone https://kazuhiloyagi@bitbucket.org/kazuhiloyagi/wisun_web.git
$ cd wisun_web
$ git checkout 0.1
```

### Dependencies
* お使いのOSの Tomcat 環境
    * Eclipse
    * jdbc ドライバ （postgresql-9.4.1212.jar）
    * com.sun.rowset.CachedRowSetImpl クラス
* PostgreSQL

### Database configuration
次に、Webアプリケーションログインアカウント登録用のテーブルを作成します。
```
CREATE TABLE m_account (
    user_id VARCHAR(24) NOT NULL, /* ユーザID */
    password VARCHAR(32) NOT NULL, /* パスワード */
    email VARCHAR(80) NOT NULL,  /* Eメールアドレス */
    update_date TIMESTAMP NOT NULL, /* 更新日時 */
    verified BOOLEAN DEFAULT FALSE, /* ユーザーのアクセス権限 */ 
    PRIMARY KEY (user_id)
);
```
作成したm_userテーブルに、ログインユーザーアカウントを1人追加します。
```
INSERT INTO m_account(user_id, password, email, verified, update_date) VALUES ('dbuser', 'dbpassword', 'hoge@example.com', true, now());
```

### How to run
Webブラウザから「http://localhost:8080/wisun-web/」にアクセスします

## Who do I talk to? ##

* kazuhiloyagi
* kazuhilo.yagi(atmark)gmail.com の(atmark)を@に変えて下さい